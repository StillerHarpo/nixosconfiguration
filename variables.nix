{
  nixosRpi4IP = "flospi";
  nixosRpi4Disk = "/dev/disk/by-id/mmc-SD128_0xb1873673";
  nixosDeckIP = "flosdeck";
  nixosDeckDisk = "/dev/disk/by-id/nvme-eui.000000000000000100a075233fefd09b";
}
