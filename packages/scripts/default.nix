pkgs: inputs:

let
  makeScript = name: substitutions:
    (pkgs.writeScriptBin name (builtins.readFile ./${name}.sh)).overrideAttrs
      (old: {
        buildCommand = ''
        ${old.buildCommand}
        patchShebangs $out
      '' + pkgs.lib.strings.concatMapStringsSep "\n" (substitution:
        "substituteInPlace $out/bin/${name} --replace '${substitution.pattern}'  '${substitution.replacement}'")
        substitutions;
      });
  mkSubstitute = pkgName: {
    pattern = "${pkgName}";
    replacement = "${pkgs."${pkgName}"}/bin/${pkgName}";
  };
  substitutes = {
    display = {
      pattern = "display";
      replacement = "${pkgs.imagemagick}/bin/display";
    };
  } // pkgs.lib.listToAttrs (map (pkgName: {
    name = "${pkgName}";
    value = mkSubstitute "${pkgName}";
  }) [
    "mpv"
    "feh"
    "curl"
    "wget"
    "zathura"
    "you-get"
    "tuir"
    "fbv"
    "w3m"
    "pass"
    "jq"
  ]);
  my-linkopenwithoutx =
    makeScript "linkopenwithoutx" (with substitutes; [ mpv wget fbv ]);
in rec {
  my-linkopenwithx = makeScript "linkopenwithx" (with substitutes; [
    mpv
    curl
    feh
    display
    wget
    zathura
    you-get
    tuir
    jq
  ]);
  my-linkopen = makeScript "linkopen" [
    {
      pattern = "./linkopenwithx.sh";
      replacement = "${my-linkopenwithx}/bin/linkopenwithx";
    }
    {
      pattern = "./linkopenwithoutx.sh";
      replacement = "${my-linkopenwithoutx}/bin/linkopenwithoutx";
    }
  ];
  rpi4-install = makeScript "rpi4-install" ([substitutes.pass] ++ [
    {
      pattern = "nixos-anywhere";
      replacement = "${inputs.nixos-anywhere.packages.x86_64-linux.nixos-anywhere}/bin/nixos-anywhere";
    }
    {
      pattern = "nixosRpi4IP";
      replacement = (import ../../variables.nix).nixosRpi4IP;
    }
    { pattern = "rpi4-uefi-firmware";
      replacement = pkgs.rpi4-uefi-firmware;
    }
  ]);
  deck-install = makeScript "deck-install" ([substitutes.pass] ++ [
    {
      pattern = "nixos-anywhere";
      replacement = "${inputs.nixos-anywhere.packages.x86_64-linux.nixos-anywhere}/bin/nixos-anywhere";
    }
    {
      pattern = "nixosDeckIP";
      replacement = (import ../../variables.nix).nixosDeckIP;
    }
  ]);
  live-streams =
    let database = pkgs.fetchurl {
           url = https://iptv-org.github.io/api/streams.json;
           hash = pkgs.lib.fakeHash;
        };
  in pkgs.writeScriptBin "live-streams" ''
      allChannels = ${pkgs.lib.getExe pkgs.jq} 'map(select(.channel)) | map(.channel) | join("\n")' ${database}
      selectedChannel = $(${pkgs.lib.getExe pkgs.fzf} -i $allChannels)
      url = $(${pkgs.lib.getExe pkgs.jq} 'map(select(.channel == $channel)) | .[0] | .url' --raw-output --arg channel $selectedChannel ${database})
      allChannels = $( $(jq 'map(select(.channel)) | map(.channel) | join("\n")'
      ${pkgs.lib.getExe pkgs.mpv} $selectedChannel
  '';

}
